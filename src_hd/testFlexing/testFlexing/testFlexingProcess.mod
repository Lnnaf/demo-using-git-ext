[Ivy]
17A14726A5D1EDAC 3.20 #module
>Proto >Proto Collection #zClass
ts0 testFlexingProcess Big #zClass
ts0 RD #cInfo
ts0 #process
ts0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
ts0 @TextInP .rdData2UIAction .rdData2UIAction #zField
ts0 @TextInP .resExport .resExport #zField
ts0 @TextInP .type .type #zField
ts0 @TextInP .processKind .processKind #zField
ts0 @AnnotationInP-0n ai ai #zField
ts0 @MessageFlowInP-0n messageIn messageIn #zField
ts0 @MessageFlowOutP-0n messageOut messageOut #zField
ts0 @TextInP .xml .xml #zField
ts0 @TextInP .responsibility .responsibility #zField
ts0 @RichDialogProcessEnd f1 '' #zField
ts0 @RichDialogProcessStart f3 '' #zField
ts0 @RichDialogEnd f4 '' #zField
ts0 @PushWFArc f5 '' #zField
ts0 @PushWFArc f2 '' #zField
ts0 @RichDialogInitStart f0 '' #zField
>Proto ts0 ts0 testFlexingProcess #zField
ts0 f1 type testFlexing.testFlexing.testFlexingData #txt
ts0 f1 53 213 22 22 14 0 #rect
ts0 f1 @|RichDialogProcessEndIcon #fIcon
ts0 f3 guid 17A14726A7D79F3B #txt
ts0 f3 type testFlexing.testFlexing.testFlexingData #txt
ts0 f3 actionDecl 'testFlexing.testFlexing.testFlexingData out;
' #txt
ts0 f3 actionTable 'out=in;
' #txt
ts0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
ts0 f3 149 85 22 22 14 0 #rect
ts0 f3 @|RichDialogProcessStartIcon #fIcon
ts0 f4 type testFlexing.testFlexing.testFlexingData #txt
ts0 f4 guid 17A14726A7EA73F3 #txt
ts0 f4 149 213 22 22 14 0 #rect
ts0 f4 @|RichDialogEndIcon #fIcon
ts0 f5 expr out #txt
ts0 f5 160 107 160 213 #arcP
ts0 f2 expr out #txt
ts0 f2 64 107 64 213 #arcP
ts0 f0 guid 17A14726A6FB5D2E #txt
ts0 f0 type testFlexing.testFlexing.testFlexingData #txt
ts0 f0 method start(testFlexing.Data) #txt
ts0 f0 disableUIEvents true #txt
ts0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<testFlexing.Data data> param = methodEvent.getInputArguments();
' #txt
ts0 f0 outParameterDecl '<testFlexing.Data data> result;
' #txt
ts0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ts0 f0 53 85 22 22 14 0 #rect
ts0 f0 @|RichDialogInitStartIcon #fIcon
>Proto ts0 .type testFlexing.testFlexing.testFlexingData #txt
>Proto ts0 .processKind HTML_DIALOG #txt
>Proto ts0 -8 -8 16 16 16 26 #rect
>Proto ts0 '' #fIcon
ts0 f0 mainOut f2 tail #connect
ts0 f2 head f1 mainIn #connect
ts0 f3 mainOut f5 tail #connect
ts0 f5 head f4 mainIn #connect
