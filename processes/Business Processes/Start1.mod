[Ivy]
17A1432D0B77F5E8 3.20 #module
>Proto >Proto Collection #zClass
S10 Start1 Big #zClass
S10 B #cInfo
S10 #process
S10 @TextInP .resExport .resExport #zField
S10 @TextInP .type .type #zField
S10 @TextInP .processKind .processKind #zField
S10 @AnnotationInP-0n ai ai #zField
S10 @MessageFlowInP-0n messageIn messageIn #zField
S10 @MessageFlowOutP-0n messageOut messageOut #zField
S10 @TextInP .xml .xml #zField
S10 @TextInP .responsibility .responsibility #zField
S10 @StartRequest f0 '' #zField
S10 @EndTask f1 '' #zField
S10 @RichDialog f2 '' #zField
S10 @PushWFArc f3 '' #zField
S10 @TaskSwitchSimple f5 '' #zField
S10 @TkArc f6 '' #zField
S10 @PushWFArc f4 '' #zField
>Proto S10 S10 Start1 #zField
S10 f0 outLink start.ivp #txt
S10 f0 type testFlexing.Data #txt
S10 f0 inParamDecl '<> param;' #txt
S10 f0 actionDecl 'testFlexing.Data out;
' #txt
S10 f0 guid 17A1432D0EA41264 #txt
S10 f0 requestEnabled true #txt
S10 f0 triggerEnabled false #txt
S10 f0 callSignature start() #txt
S10 f0 caseData businessCase.attach=true #txt
S10 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
S10 f0 @C|.responsibility Everybody #txt
S10 f0 97 49 30 30 -21 17 #rect
S10 f0 @|StartRequestIcon #fIcon
S10 f1 type testFlexing.Data #txt
S10 f1 585 49 30 30 0 15 #rect
S10 f1 @|EndIcon #fIcon
S10 f2 targetWindow NEW #txt
S10 f2 targetDisplay TOP #txt
S10 f2 richDialogId testFlexing.testFlexing #txt
S10 f2 startMethod start(testFlexing.Data) #txt
S10 f2 type testFlexing.Data #txt
S10 f2 requestActionDecl '<testFlexing.Data data> param;' #txt
S10 f2 requestMappingAction 'param.data=in;
' #txt
S10 f2 responseActionDecl 'testFlexing.Data out;
' #txt
S10 f2 responseMappingAction 'out=result.data;
' #txt
S10 f2 isAsynch false #txt
S10 f2 isInnerRd false #txt
S10 f2 userContext '* ' #txt
S10 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Start1 process</name>
        <nameStyle>14,7
</nameStyle>
    </language>
</elementInfo>
' #txt
S10 f2 312 52 32 24 20 -2 #rect
S10 f2 @|RichDialogIcon #fIcon
S10 f3 expr out #txt
S10 f3 127 64 312 64 #arcP
S10 f5 actionDecl 'testFlexing.Data out;
' #txt
S10 f5 actionTable 'out=in1;
' #txt
S10 f5 outTypes "testFlexing.Data" #txt
S10 f5 outLinks "TaskA.ivp" #txt
S10 f5 taskData 'TaskA.EXPRI=2
TaskA.EXROL=Everybody
TaskA.EXTYPE=0
TaskA.NAM=SAMPLE TASK FROM LINH''S TESTING
TaskA.PRI=2
TaskA.ROL=Everybody
TaskA.SKIP_TASK_LIST=false
TaskA.TYPE=0' #txt
S10 f5 type testFlexing.Data #txt
S10 f5 template "" #txt
S10 f5 483 51 26 26 13 0 #rect
S10 f5 @|TaskSwitchSimpleIcon #fIcon
S10 f6 expr out #txt
S10 f6 type testFlexing.Data #txt
S10 f6 var in1 #txt
S10 f6 344 64 483 64 #arcP
S10 f4 expr data #txt
S10 f4 outCond ivp=="TaskA.ivp" #txt
S10 f4 509 64 585 64 #arcP
>Proto S10 .type testFlexing.Data #txt
>Proto S10 .processKind NORMAL #txt
>Proto S10 0 0 32 24 18 0 #rect
>Proto S10 @|BIcon #fIcon
S10 f0 mainOut f3 tail #connect
S10 f3 head f2 mainIn #connect
S10 f2 mainOut f6 tail #connect
S10 f6 head f5 in #connect
S10 f5 out f4 tail #connect
S10 f4 head f1 mainIn #connect
