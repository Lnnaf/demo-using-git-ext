package testFlexing.testFlexing;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class testFlexingData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class testFlexingData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -5551561112036699216L;

  private testing.bean.Person bean;

  /**
   * Gets the field bean.
   * @return the value of the field bean; may be null.
   */
  public testing.bean.Person getBean()
  {
    return bean;
  }

  /**
   * Sets the field bean.
   * @param _bean the new value of the field bean.
   */
  public void setBean(testing.bean.Person _bean)
  {
    bean = _bean;
  }

}
