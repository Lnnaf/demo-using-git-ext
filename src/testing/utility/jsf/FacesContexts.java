package testing.utility.jsf;

import java.io.IOException;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ELException;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.ClassUtils;
import org.primefaces.context.RequestContext;

import testing.exception.FunkeException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.util.IvyRuntimeException;


public class FacesContexts {
	private FacesContexts() {
	}

	/**
	 * Find a bean with given expression.
	 * 
	 * @param expression
	 * @param clazz
	 * @return a according bean
	 */
	public static <C> C evaluateValueExpression(String expression, Class<C> clazz) {
		FacesContext fc = FacesContext.getCurrentInstance();
		if (fc == null || fc.getApplication() == null) {
			return null;
		}

		return fc.getApplication().evaluateExpressionGet(fc, expression, clazz);
	}

	/**
	 * Invoke method of another bean from which bean called this method by
	 * method expression string</br> Exp: #{logic.plusTwoRealNumber}
	 * 
	 * @param methodExpressionLiteral
	 * 
	 * @param parameters
	 *            array of parameter of method
	 * @param returnedType
	 *            expected returned type of method calling
	 * @return return value
	 * @throws ELException
	 *             if method expression string is invalid
	 */
	public static <E> E invokeMethodByExpression(String methodExpressionLiteral, Object[] parameters, Class<E> returnedType) {
		ELContext elContext = getELContext();
		Application application = getApplication();
		ExpressionFactory expressionFactory = application.getExpressionFactory();
		MethodExpression methodExpression = expressionFactory.createMethodExpression(elContext, methodExpressionLiteral, returnedType, ClassUtils.toClass(parameters));
		 
		return invokeMethod(elContext, methodExpression, parameters, returnedType);
	}

	/**
	 * Get faceContext's application
	 * 
	 * @return application
	 */
	public static Application getApplication() {
		return getCurrentInstance().getApplication();
	}

	/**
	 * Get EL context.
	 * 
	 * @return current EL context
	 */
	public static ELContext getELContext() {
		return getCurrentInstance().getELContext();
	}

	/**
	 * Get current instance.
	 * 
	 * @return current instance
	 */
	private static FacesContext getCurrentInstance() {
		return FacesContext.getCurrentInstance();
	}

	/**
	 * Get external context.
	 * 
	 * @return external context.
	 */
	private static ExternalContext getExternalContext() {
		return getCurrentInstance().getExternalContext();
	}

	@SuppressWarnings("unchecked")
	private static <E> E invokeMethod(ELContext elContext, MethodExpression methodExpression, Object[] parameters, Class<E> returnedType) {
		try {
			Object result = methodExpression.invoke(elContext, parameters);
			return returnedType != null ? returnedType.cast(result) : (E) result;
		} catch (ELException e) {
			Ivy.log().error(e);
			if(e.getCause() instanceof IvyRuntimeException){
				throw e;
			}

			throw new FunkeException("Cannot invoke method expression", e);
		} catch (ClassCastException ex) {
			throw new FunkeException("The data type of return value is not as expected", ex);
		}
	}

	/**
	 * Get request parameter value.
	 * 
	 * @param key
	 * @return value
	 */
	public static String getRequestParameterValue(String key) {
		Map<String, String> requestParameterMap = getRequestParameterMap();
		return requestParameterMap.get(key);
	}

	/**
	 * Get request parameter map.
	 * 
	 * @return parameter map
	 */
	private static Map<String, String> getRequestParameterMap() {
		return getExternalContext().getRequestParameterMap();
	}

	/**
	 * Redirect to given URL.
	 * 
	 * @param url
	 */
	public static void redirect(String url) {
		try {
			getExternalContext().redirect(url);
		} catch (IOException e) {
			String error = String.format("Cannot redirect to the given url: %s", url);
			throw new FunkeException(error, e);
		}
	}

	public static void executeJSScript(String script) {
		RequestContext.getCurrentInstance().execute(script);
	}
}
