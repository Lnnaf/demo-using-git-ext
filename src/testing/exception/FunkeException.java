package testing.exception;



/**
 * FunkeException
 * 
 * @author maonguyen
 *
 */
public class FunkeException extends RuntimeException {

	/**
	 * Default serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public FunkeException() {
		super();
	}
	
	/**
	 * @param message
	 */
	public FunkeException(String message) {
		super(message);
	}
	
	/**
	 * @param throwable
	 */
	public FunkeException(Throwable throwable) {
		super(throwable);
	}
	
	/**
	 * @param message
	 * @param throwable
	 */
	public FunkeException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
