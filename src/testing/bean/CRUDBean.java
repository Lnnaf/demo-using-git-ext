package testing.bean;

import testing.utility.jsf.FacesContexts;

public abstract class CRUDBean<T> {
	protected boolean createNew;

	public abstract void init();

	public abstract void loadEntries();

	public abstract void onCreate();

	public abstract void handleCreate();

	public abstract String getTableName();

	public abstract void handleUpdate();

	public void handleDelete() {}

	public void refilterTableOnUI() {
		FacesContexts.executeJSScript("PF('" + getTableName() + "').filter();");
	}

	public boolean isCreateNew() {
		return createNew;
	}

	public void setCreateNew(boolean createNew) {
		this.createNew = createNew;
	}
}
